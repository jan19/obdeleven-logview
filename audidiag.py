#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Copyright (C) 2020  Jan Grønlien

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

class Fault:

  def __init__(self, id, description):
    self.id = id
    self.description = description


class ControlUnit:

  def __init__(self, unitId, unitDescription):
    self.unitId = unitId
    self.unitDescription = unitDescription
    self.systemDescription = None
    self.swNumber = None
    self.swVersion = None
    self.hwNumber = None
    self.hwVersion = None
    self.faults = []

  def __str__(self):
    return('%s, %s, %s, %s, %s, %s, %s' % (self.unitId, self.unitDescription, self.systemDescription, self.swNumber, self.swVersion, self.hwNumber, self.hwVersion))
  
  def addFault(self, fault):
    self.faults.append(fault)

class FullScan:
  def __init__(self):
    self.control_units_count = 0
    self.faults_count = 0
    self.control_units = []

class ScanLog:
  def __init__(self):
    self.full_scan = []
    self.control_units = []
    self.date = None
    self.vin = None
    self.car = None
    self.body_type = None
    self.engine = None
    self.mileage = 0

  def addFullScan(self, fullScan):
    self.full_scan.append(fullScan)
