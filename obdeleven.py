#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Copyright (C) 2020  Jan Grønlien

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from audidiag import *
import re
import sqlite3
import sys

def usage():
  print('usage:')
  print('obdeleven.py logfile')

def cu_sort_key(cu):
  return(int(cu.unitId, 16))

def get_value(line):
  return(line.strip().split(':')[1].strip())

def main(inputfile):
  with open(inputfile, 'r') as logfile:
    scan_log = ScanLog()
    full_scan = None
    control_unit = None
    for line in logfile:
      if line.startswith('Full scan'):
        if control_unit != None:
          if full_scan != None:
            # Append control unit to scan log
            full_scan.control_units.append(control_unit)
          else:
            # Append control unit to full scan
            scan_log.control_units.append(control_unit)
        if full_scan != None:
          scan_log.addFullScan(full_scan)
        full_scan = FullScan()
      elif line.startswith('\tControl Units:'):
        c = line.split(':')[1].strip()
        if c != '':
          full_scan.control_units_count = c
      elif re.search('^\t\t[0-F][0-F] .*', line):
        # start control unit in scan
        sline = line.strip()
        systemId = sline[0:2]
        systemDescription = sline[3:]
        if control_unit != None:
          full_scan.control_units.append(control_unit)
        control_unit = ControlUnit(systemId, systemDescription)
      elif re.search('^[0-F][0-F] .*', line):
        # start global control unit in log
        sline = line.strip()
        systemId = sline[0:2]
        systemDescription = sline[3:]
        if control_unit != None:
          scan_log.control_units.append(control_unit)
        control_unit = ControlUnit(systemId, systemDescription)
      elif line.startswith('\t\tSystem description:'):
        control_unit.systemDescription = get_value(line)
      elif line.startswith('\t\tSoftware number:'):
        control_unit.swNumber = get_value(line)
      elif line.startswith('\t\tSoftware version:'):
        control_unit.swVersion = get_value(line)
      elif line.startswith('\t\tHardware number:'):
        control_unit.hwNumber = get_value(line)
      elif line.startswith('\t\tHardware version:'):
        control_unit.hwVersion = get_value(line)

    # Create in memory database
    db = sqlite3.connect(':memory:')
    cur = db.cursor()
    cur.execute('''create table control_unit (
      scan_no number, 
      unit_id text, 
      unit_description text, 
      system_description text, 
      sw_number text, 
      sw_version text, 
      hw_number text,
      hw_version text)''')

    scan_no = 0
    for fs in scan_log.full_scan:
      scan_no += 1
      fs.control_units.sort(key=cu_sort_key)
      for cu in fs.control_units:
        #print('%d, %s' % (scan_no, cu))
        try:
          sql = 'insert into control_unit values (?, ?, ?, ?, ?, ?, ?, ?)' 
          cur.execute(sql, (scan_no, cu.unitId, cu.unitDescription, cu.systemDescription, cu.swNumber, cu.swVersion, cu.hwNumber, cu.hwVersion))
        except sqlite3.OperationalError as e:
          print(e)
          print(sql)
    db.commit()
    version_sql = '''select 
min(scan_no),
unit_id, 
unit_description, 
system_description, 
sw_number, 
sw_version, 
hw_number, 
hw_version
from control_unit
group by unit_id, unit_description, system_description, sw_number, sw_version, hw_number, hw_version
order by unit_id, scan_no'''
    cur.execute(version_sql)
    versions = cur.fetchall()
    format = '%4s %-3s %-24s %-16s %-11s %-4s %-11s %s'
    print(format % ('Scan', 'CU', 'Description', 'System Desc', 'sw number', 'ver', 'hw number', 'ver'))
    print(format % ('----', '---', '------------------------', '----------------', '-----------', '----', '-----------', '---'))
    for cu in versions:
      print(format % (cu))

if __name__ == "__main__":
  if len(sys.argv) != 2:
    usage()
    sys.exit(-1)
  main(sys.argv[1])