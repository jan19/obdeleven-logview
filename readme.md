### obdeleven.py
Parse output from OBDEleven and display history for hardware components
Developed and tested using logfile from OBDEleven 1.2.1 for iOS

# Usage (03 brakes ar showing an updated version, detected in scan no 5, from the logfile)
```
obdeleven.py OBDeleven_Log.txt
Scan CU  Description              System Desc      sw number   ver  hw number   ver
---- --- ------------------------ ---------------- ----------- ---- ----------- ---
   1 01  Engine                   I99 99.9l XXX    4KE907558A  0001 4KE907558   H02
   1 03  Brakes                   ESC              4KE909059AD 0231 4KE909059N  H11
   5 03  Brakes                   ESC              4KE909059AD 0231 4KE909059N  H12
   1 09  Central Electrics        BCM1 MLBevo      4N0907063KG 0691 4N0907063AG H10
```